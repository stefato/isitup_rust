USAGE:
    isitup \<URL>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <URL>    URL to be checked
