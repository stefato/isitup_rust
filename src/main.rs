#[macro_use]
extern crate clap;
extern crate reqwest;
extern crate url;

use clap::{App, Arg};
use serde::Deserialize;
use url::Url;

const BASE_SCHEMA: &str = "http://";
const IS_IT_UP_URL: &str = "https://isitup.org";

fn main() {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about("Checks if website is online")
        .arg(Arg::with_name("URL")
            .help("URL to be checked")
            .required(true)
            .index(1)
            .validator(is_url))
        .get_matches();

    let domain = matches.value_of("URL").unwrap();

    match check_is_it_up(domain) {
        Ok(message) => println!("{}", message),
        Err(error_message) => println!("{}", error_message),
    };
}


fn is_url(val: String) -> Result<(), String> {
    let prepared_input = prepend_http(&val);

    match Url::parse(&prepared_input) {
        Ok(_) => Ok(()),
        Err(_) => Err(String::from("Not a valid Url"))
    }
}

fn prepend_http(val: &str) -> String {
    if !val.starts_with(BASE_SCHEMA) { BASE_SCHEMA.to_owned() + val } else { val.to_string() }
}

#[derive(Debug, Deserialize)]
struct IsItUpResp {
    domain: String,
    port: Option<u16>,
    status_code: i8,
    response_ip: Option<String>,
    response_code: Option<u16>,
    response_time: Option<f64>,
}

fn check_is_it_up(input: &str) -> Result<String, String> {
    let input = prepend_http(input);

    // already validated
    let url_to_check = Url::parse(&input).unwrap();

    if !url_to_check.has_host() {
        return Err(String::from("Invalid Url"));
    }

    // guaranteed to be valid
    let is_it_up_url = Url::parse(IS_IT_UP_URL).unwrap();
    let host_to_check = url_to_check.host_str().unwrap();

    let path = format!("{}.json", host_to_check);


    // https://isitup.org/host_to_check.json"
    let request_url = is_it_up_url.join(&path).unwrap();

    let result = reqwest::get(request_url)
        .and_then(|mut response| response.json())
        .map(|is_it_up_resp: IsItUpResp| is_it_up_resp.status_code)
        .map_err(|_| String::from("Can't connect to isitup.org"));

    match result {
        Ok(1) => Ok(String::from(format!("{} is online", host_to_check))),
        Ok(2) => Ok(String::from(format!("{} is offline", host_to_check))),
        Ok(3) => Err(String::from("Invalid url")),
        Ok(_) => Err(String::from("Invalid server response")),
        Err(error_message) => Err(error_message),
    }
}